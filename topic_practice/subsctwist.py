import rclpy
from geometry_msgs.msg import Twist
from std_msgs.msg import Float64
from time import sleep

def callback_twist(msg):
    print('escucho: '+ str(msg.linear.x))

def main(args=None):
    #crear el nodo
    rclpy.init(args=args)
    node = rclpy.create_node('subsctwist')
    #subscriptor
    subscriptor = node.create_subscription(Twist,'/cmd_vel',callback_twist,10)
    #mantener el nodo vivo
    rclpy.spin(node)
    #destruir nodo y comms
    node.destroy_node
    rclpy.shutdown


if __name__ == "__main__":
    main()

