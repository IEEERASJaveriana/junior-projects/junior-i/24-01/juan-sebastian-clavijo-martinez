import rclpy
from std_msgs.msg import Int64
from time import sleep
from turtlesim.msg import Pose

def callback_string(msg):
    print(msg.data)

def callback_pose(msg):
    msg_p='x: '+str(msg.x)+' y: '+str(msg.y)
    print(msg_p)

def main(args=None):
    #crear el nodo
    rclpy.init(args=args)
    node = rclpy.create_node('subscriptor')
    #subscriptor
    subscriptor = node.create_subscription(Int64,'str_topic',callback_string,10)
    #subscriptor_turtlesim = node.create_subscription(Pose, '/turtle1/pose', callback_pose,10)
    #mantener el nodo vivo
    rclpy.spin(node)
    #destruir nodo y comms
    node.destroy_node
    rclpy.shutdown


if __name__ == "__main__":
    main()