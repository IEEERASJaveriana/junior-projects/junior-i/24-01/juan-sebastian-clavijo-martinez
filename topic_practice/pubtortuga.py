import rclpy
from std_msgs.msg import String
from time import sleep
from geometry_msgs.msg import Twist
from std_msgs.msg import Float64

def mimensaje(x, y, z):
    msg = Twist()
    msg.linear.x=x
    msg.linear.y=y
    msg.linear.z=z
    return msg

def main(args=None):
    #crear el nodo
    rclpy.init(args=args)
    node = rclpy.create_node('torpublisher')
    #publicador Twist
    publicador = node.create_publisher(Twist,'/turtle1/cmd_vel',10)
    contador=0.0
    while rclpy.ok:
        mensaje = mimensaje(contador*0.1,0.1,0.1)
        publicador.publish(mensaje)
        print('X: '+str(mensaje.linear.x)+' Y: '+str(mensaje.linear.y)+' Z: '+str(mensaje.linear.z))
        contador+=1.0
        sleep(1)
    rclpy.spin()
    node.destroy_node
    rclpy.shutdown()


if __name__ == "__main__":
    main()