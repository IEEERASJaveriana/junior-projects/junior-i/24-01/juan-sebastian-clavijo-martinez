import rclpy
from std_msgs.msg import Int64
from time import sleep

def main(args=None):
    #crear el nodo
    rclpy.init(args=args)
    node = rclpy.create_node('publicadorI64')
    #publicador
    publicador = node.create_publisher(Int64,'str_topic',10)
    contador=0 #esto es una guachada
    mensaje = Int64()
    while rclpy.ok:
        mensaje.data = contador
        contador +=1
        publicador.publish(mensaje)
        print('acabo de publicar: '+str(contador))
        sleep(1)

    #mantener el nodo vivo
    rclpy.spin()
    #destruir nodo y comms
    node.destroy_node
    rclpy.shutdown


if __name__ == "__main__":
    main()