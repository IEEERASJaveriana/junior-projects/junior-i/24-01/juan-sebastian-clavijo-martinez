import rclpy
from geometry_msgs.msg import Twist
from std_msgs.msg import Float64
from time import sleep

def mimensaje(x, y, z):
    msg = Twist()
    msg.linear.x=x
    msg.linear.y=y
    msg.linear.z=z
    return msg



def main(args=None):
    #crear el nodo
    rclpy.init(args=args)
    node = rclpy.create_node('pubtwist')
    #publicador Twist
    publicador = node.create_publisher(Twist,'cmd_vel',10)
    contador=0.0
    while rclpy.ok:
        mensaje = mimensaje(contador,2.0,3.0)
        publicador.publish(mensaje)
        print('X: '+str(mensaje.linear.x)+' Y: '+str(mensaje.linear.y)+' Z: '+str(mensaje.linear.z))
        contador+=1.0
        sleep(1)
    rclpy.spin()
    node.destroy_node
    rclpy.shutdown()

if __name__ == '__main__':
    main()
